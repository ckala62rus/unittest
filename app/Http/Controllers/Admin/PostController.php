<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\PostRequest;
use App\Services\PostService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * @var PostService
     */
    public $postService;

    /**
     * PostController constructor.
     * @param PostService $postService
     */
    public function __construct(
        PostService $postService
    ) {
        $this->postService = $postService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $limit = $request->get('limit');

        $posts = $this
            ->postService
            ->getAllPosts($limit);

        return response()->json($posts, JsonResponse::HTTP_OK);
    }

    /**
     * @param PostRequest $request
     * @return JsonResponse
     */
    public function store(PostRequest $request): JsonResponse
    {
        $data = $request->all();

        $post = $this
            ->postService
            ->createNewPost($data);

        return response()->json($post, JsonResponse::HTTP_CREATED);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $model = $this
            ->postService
            ->getOneRecord($id);

        return response()->json($model, JsonResponse::HTTP_OK);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $model = $this
            ->postService
            ->destroyOnePost($id);

        return response()->json($model, JsonResponse::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $data = $request->all();

        $model = $this
            ->postService
            ->updateOneRecordById($data, $id);

        return response()->json($model, JsonResponse::HTTP_OK);
    }
}
