<?php

namespace App\Repositories;

class AbstractRepository
{
    public $model;

    public function newQuery()
    {
        return $this
            ->model
            ->newQuery();
    }
}
