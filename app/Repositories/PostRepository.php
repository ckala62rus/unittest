<?php

namespace App\Repositories;

use App\Models\Post;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

class PostRepository extends AbstractRepository
{
    /**
     * PostRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Post();
    }

    /**
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        return $this
            ->newQuery()
            ->create($data);
    }

    /**
     * @param int $id
     * @return Model
     */
    public function getOneRecord(int $id): Model
    {
        return $this
            ->newQuery()
            ->findOrFail($id);
    }

    /**
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function getAllRecords(int $limit): LengthAwarePaginator
    {
        return $this
            ->model
            ->newQuery()
            ->paginate($limit);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function destroyOneRecord(int $id): bool
    {
        return $this
            ->model
            ->destroy($id);
    }

    /**
     * @param array $array
     * @param int $id
     * @return Model
     */
    public function update(array $array, int $id): Model
    {
        $model = $this
            ->model
            ->findOrFail($id);

        $model->update($array);

        return $model;
    }
}
