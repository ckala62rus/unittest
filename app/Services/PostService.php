<?php

namespace App\Services;

use App\Repositories\PostRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

class PostService
{
    /**
     * @var PostRepository
     */
    public $postRepository;

    /**
     * PostService constructor.
     * @param PostRepository $postRepository
     */
    public function __construct(
        PostRepository $postRepository
    ) {
        $this->postRepository = $postRepository;
    }

    /**
     * @param array $data
     * @return Model
     */
    public function createNewPost(array $data): Model
    {
        return $this
            ->postRepository
            ->create($data);
    }

    /**
     * @param int $id
     * @return Model
     */
    public function getOneRecord(int $id): Model
    {
        return $this
            ->postRepository
            ->getOneRecord($id);
    }

    /**
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function getAllPosts(int $limit): LengthAwarePaginator
    {
        return $this
            ->postRepository
            ->getAllRecords($limit);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function destroyOnePost(int $id): bool
    {
        return $this
            ->postRepository
            ->destroyOneRecord($id);
    }

    /**
     * @param array $array
     * @param int $id
     * @return Model
     */
    public function updateOneRecordById(array $array, int $id): Model
    {
        return $this
            ->postRepository
            ->update($array, $id);
    }
}
