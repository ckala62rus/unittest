<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Services\PostService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;

    public $postService = PostService::class;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreatePost()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('api/posts', [
            'title' => 'test',
            'description' => 'message...',
        ]);

        $post = $response->assertCreated();

        $this->assertEquals('test', $post['title']);
        $this->assertEquals('message...', $post['description']);

        $response->assertStatus(201);

        $post = (new Post())
            ->first();

        $this->assertEquals(1, $post->id);
        $this->assertEquals('test', $post->title);
        $this->assertEquals('message...', $post->description);
    }

    public function testDeletePost()
    {
        $this->withoutExceptionHandling();

        $postModel = $this->delete('api/posts/2');

        $this->assertEquals(200, $postModel->status());
        $this->assertEquals('false', $postModel->content());

        $data = [
            'title' => 'test',
            'description' => 'message...',
        ];

        $post = (new Post())
            ->newQuery()
            ->create($data);

        $this->assertEquals(1, $post->id);
        $this->assertEquals('test', $post['title']);
        $this->assertEquals('message...', $post['description']);

        $postModel = $this->delete('api/posts/1');

        $this->assertEquals('true', $postModel->content());
    }

    public function testOneRecord()
    {
        $response = $this->get('api/posts/1');

        $this->assertEquals(404, $response->getStatusCode());

        $data = [
            'title' => 'test',
            'description' => 'message...',
        ];

        $post = (new Post())
            ->newQuery()
            ->create($data);

        $this->assertEquals(1, $post->id);
        $this->assertEquals('test', $post['title']);
        $this->assertEquals('message...', $post['description']);

        $response = $this->get('api/posts/1');

        $this->assertEquals(1, $response->getOriginalContent()->id);
        $this->assertEquals('test', $response->getOriginalContent()->title);
        $this->assertEquals('message...', $response->getOriginalContent()->description);
    }

    public function testGetAllPosts()
    {
        $data = [
            [
                'id' => 1,
                'title' => 'test',
                'description' => 'message...',
            ],
            [
                'id' => 2,
                'title' => 'test2',
                'description' => 'message2...',
            ],
        ];

        foreach ($data as $dateForCreatePost) {
            (new Post())->newQuery()->create($dateForCreatePost);
        }

        $posts = (new Post())->all();

        $i = 0;

        foreach ($posts as $post) {
            $this->assertEquals($data[$i]['id'], $post->id);
            $this->assertEquals($data[$i]['title'], $post->title);
            $this->assertEquals($data[$i]['description'], $post->description);
            $i++;
        }
    }

    public function testUpdateOneRecordById()
    {
        $data = [
            'title' => 'Hello World!',
            'description' => 'Show me the money',
        ];

        $response = $this->put('api/posts/1', $data);

        $this->assertEquals(404, $response->getStatusCode());

        $this->testCreatePost();

        $response = $this->put('api/posts/1', $data);

        $post = $response->getOriginalContent();

        $this->assertEquals(1, $post->id);
        $this->assertEquals('Hello World!', $post->title);
        $this->assertEquals('Show me the money', $post->description);

        $this->assertEquals(200, $response->getStatusCode());
    }
}

//vendor/bin/phpunit --filter ExampleTest tests/Feature
